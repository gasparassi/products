<?php

namespace Tests\Unit\Services;

use App\Models\Product;
use App\Repositories\Contracts\ProductRepositoryInterface;
use App\Services\ProductService;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;

class ProductServiceTest extends TestCase
{
    use WithFaker;

    private $productRepository;
    private $productService;
    private $productModel;

    protected function setUp(): void
    {
        parent::setUp();
        $this->productRepository = $this->getMockBuilder(ProductRepositoryInterface::class)
            ->disableOriginalConstructor()
            ->disableOriginalClone()
            ->getMock();
        $this->productService = new ProductService($this->productRepository);

        $this->productModel = $this->createProduct(5);

        $this->setUpFaker();
    }

    private function createProduct(int $category_id): Product
    {
        $product = new Product();
        $product->id = $this->faker->numberBetween(1, 15);
        $product->name = $this->faker->name;
        $product->active = true;
        $product->category_id = $category_id;

        return $product;
    }

    private function createProductsList(int $category_id): array
    {
        $list = [];
        for ($i = 0; $i < 10; $i++) {
            $list[] = $this->createProduct($category_id);
        }
        return $list;
    }

    /**
     * @test
     */
    public function shouldBeCreateANewProduct(): void
    {
        $this->productRepository->expects($this->once())
            ->method('createProduct')
            ->with($this->productModel->toArray())
            ->willReturn($this->productModel);
        $productCreated = $this->productService->create($this->productModel->toArray());
        $this->assertNotNull($productCreated);
        $this->assertInstanceOf(Product::class, $productCreated);
        $this->assertSame($this->productModel, $productCreated);
    }

    /**
     * @test
     */
    public function shouldBeNotCreateANewProduct(): void
    {
        $this->assertIsNotArray($this->productModel);
        $this->productRepository->expects($this->never())
            ->method('createProduct')
            ->with(null)
            ->willReturn(null);
    }

    /**
     * @test
     */
    public function shouldBeReturnProductsList(): void
    {
        $this->productRepository->expects($this->once())
            ->method('getAllProducts')
            ->willReturn($this->createProductsList(5));

        $products = $this->productService->getAll();

        $this->assertNotNull($products);
        $this->assertIsArray($products);
        $this->assertEquals(true, count($products) > 0);
    }

    /**
     * @test
     */
    public function shouldBeNotReturnProductsList(): void
    {
        $this->productRepository->expects($this->once())
            ->method('getAllProducts')
            ->willReturn(null);

        $products = $this->productService->getAll();

        $this->assertNull($products);
    }

    /**
     * @test
     */
    public function shouldBeReturnAOneProduct(): void
    {
        $this->productRepository->expects($this->once())
            ->method('getProductById')
            ->with($this->productModel->id)
            ->willReturn($this->createProduct($this->productModel->id));

        $productOne = $this->productService->getOne($this->productModel->id);

        $this->assertNotNull($productOne);
        $this->assertInstanceOf(Product::class, $productOne);
        $this->assertNotSame($this->productModel, $productOne);
    }

    /**
     * @test
     */
    public function shouldBeUpdateAOneProduct(): void
    {
        $this->productRepository->expects($this->once())
            ->method('getProductById')
            ->with($this->productModel->id)
            ->willReturn($this->productModel);

        $this->productRepository->expects($this->once())
            ->method('updateProduct')
            ->with($this->productModel)
            ->willReturn($this->productModel);

        $productUpdated = $this->productService->updateProduct($this->productModel->id, $this->productModel->toArray());
        $this->assertNotNull($productUpdated);
        $this->assertInstanceOf(Product::class, $productUpdated);
        $this->assertSame($this->productModel, $productUpdated);

    }

    /**
     * @test
     */
    public function shouldBeNotUpdateAOneProductBecauseProductIsNull(): void
    {
        $this->productRepository->expects($this->once())
            ->method('getProductById')
            ->with(0)
            ->willReturn(null);

        $this->productRepository->expects($this->never())
            ->method('updateProduct')
            ->with(null)
            ->willReturn(null);

        $productUpdated = $this->productService->updateProduct(0, []);
        $this->assertNull($productUpdated);

    }

    /**
     * @test
     */
    public function shouldBeDeleteAOneProduct(): void
    {
        $this->productRepository->expects($this->once())
            ->method('getProductById')
            ->with($this->productModel->id)
            ->willReturn($this->productModel);

        $this->productRepository->expects($this->once())
            ->method('deleteProduct')
            ->with($this->productModel)
            ->willReturn(true);

        $productDeleted = $this->productService->delete($this->productModel->id);
        $this->assertTrue($productDeleted);

    }

    /**
     * @test
     */
    public function shouldBeNotDeleteAOneProductBecauseProductIsNull(): void
    {
        $this->productRepository->expects($this->once())
            ->method('getProductById')
            ->with(0)
            ->willReturn(null);

        $this->productRepository->expects($this->never())
            ->method('deleteProduct')
            ->with(null)
            ->willReturn(false);

        $productDeleted = $this->productService->delete(0);
        $this->assertFalse($productDeleted);

    }

    /**
     * @test
     */
    public function shouldBeReturnAllProductsByStatus(): void
    {
        $valueStatus = true;
        $this->productRepository->expects($this->once())
            ->method('getAllProductsByStatusStatus')
            ->with($valueStatus)
            ->willReturn($this->createProductsList(5));

        $productsByStatus = $this->productService->getAllStatus($valueStatus);
        $this->assertNotNull($productsByStatus);
        $this->assertEquals(true, count($productsByStatus) > 0);

    }

    /**
     * @test
     */
    public function shouldBeUpdateStatusProduct(): void
    {
        $valueStatus = false;
        $this->productRepository->expects($this->once())
            ->method('getProductById')
            ->with($this->productModel->id)
            ->willReturn($this->productModel);
        $this->productRepository->expects($this->once())
            ->method('updateStatusProduct')
            ->with($this->productModel)
            ->willReturn($this->productModel);

        $productUpdated = $this->productService->updateStatusProduct($this->productModel->id, $valueStatus);
        $this->assertNotNull($productUpdated);
        $this->assertInstanceOf(Product::class, $productUpdated);
        $this->assertSame($this->productModel, $productUpdated);
    }

    /**
     * @test
     */
    public function shouldBeNotUpdateStatusProductBecauseProductIsNull(): void
    {
        $valueStatus = false;
        $this->productRepository->expects($this->once())
            ->method('getProductById')
            ->with(0)
            ->willReturn(null);
        $this->productRepository->expects($this->never())
            ->method('updateStatusProduct')
            ->with(null)
            ->willReturn(null);

        $productUpdated = $this->productService->updateStatusProduct(0, $valueStatus);
        $this->assertNull($productUpdated);
        $this->assertNotInstanceOf(Product::class, $productUpdated);
        $this->assertNotSame($this->productModel, $productUpdated);
    }

}
