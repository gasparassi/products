<?php

namespace Tests\Unit\Services;

use App\Models\Category;
use App\Models\Product;
use App\Repositories\Contracts\CategoryRepositoryInterface;
use App\Services\CategoryService;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;

class CategoryServiceTest extends TestCase
{

    use WithFaker;

    private $categoryRepository;
    private $categoryService;
    private $categoryModel;

    public function setUp(): void
    {
        parent::setUp();
        $this->categoryRepository = $this->getMockBuilder(CategoryRepositoryInterface::class)
            ->disableOriginalConstructor()
            ->disableOriginalClone()
            ->getMock();

        $this->categoryService = new CategoryService($this->categoryRepository);

        $this->categoryModel = $this->createCategory();

        $this->setUpFaker();
    }

    private function createCategory(): Category
    {
        $category = new Category();
        $category->id = $this->faker->numberBetween(1, 15);
        $category->name = $this->faker->name;
        $category->active = true;
        $category->products = $this->createProductsList($category->id);

        return $category;
    }

    private function createProduct(int $category_id): Product
    {
        $product = new Product();
        $product->id = $this->faker->numberBetween(1, 15);
        $product->name = $this->faker->name;
        $product->active = true;
        $product->category_id = $category_id;

        return $product;
    }

    private function createCategoryList(): array
    {
        $list = [];
        for ($i = 0; $i < 5; $i++) {
            $list[] = $this->createCategory();
        }
        return $list;
    }

    private function createProductsList(int $category_id): array
    {
        $list = [];
        for ($i = 0; $i < 10; $i++) {
            $list[] = $this->createProduct($category_id);
        }
        return $list;
    }

    private function setCategory($id, $return)
    {
        $this->categoryRepository->expects($this->once())
            ->method('getCategoryById')
            ->with($id)
            ->willReturn($return);

        return $this;
    }

    /**
     * @test
     */
    public function shouldBeCreateANewCategory(): void
    {
        $this->categoryRepository->expects($this->once())
            ->method('createCategory')
            ->with($this->categoryModel->toArray())
            ->willReturn($this->categoryModel);

        $newCategory = $this->categoryService->create($this->categoryModel->toArray());
        $this->assertNotNull($newCategory);
        $this->assertInstanceOf(Category::class, $newCategory);
        $this->assertSame($this->categoryModel, $newCategory);

    }

    /**
     * @test
     */
    public function shouldBeNotCreateANewCategory(): void
    {
        $this->assertIsNotArray($this->categoryModel);
        $this->categoryRepository->expects($this->never())
            ->method('createCategory')
            ->with(null)
            ->willReturn(null);
    }

    /**
     * @test
     */
    public function shouldBeUpdateAOneCategory(): void
    {
        $category_id = $this->categoryModel->id;
        $this->setCategory($category_id, $this->categoryModel);

        $this->categoryRepository->expects($this->once())
            ->method('updateCategory')
            ->with($this->categoryModel)
            ->willReturn($this->categoryModel);

        $updatedCategory = $this->categoryService->updateCategory(
            $category_id, $this->categoryModel->toArray()
        );

        $this->assertNotNull($updatedCategory);
        $this->assertInstanceOf(Category::class, $updatedCategory);
        $this->assertSame($this->categoryModel, $updatedCategory);
    }

    /**
     * @test
     */
    public function shouldBeNotUpdateAOneCategory(): void
    {
        $this->categoryRepository->expects($this->never())
            ->method('updateCategory')
            ->with(null)
            ->willReturn(null);
    }

    /**
     * @test
     */
    public function shouldBeNotUpdateAOneCategoryBecauseCategoryIsNull(): void
    {
        $this->setCategory(0, null);
        $this->categoryRepository->expects($this->never())
            ->method('updateCategory')
            ->with(null)
            ->willReturn(null);

        $categoryUpdated = $this->categoryService->updateCategory(0, []);
        $this->assertNull($categoryUpdated);
    }

    /**
     * @test
     */
    public function shouldBeReturnAllCategories(): void
    {
        $this->categoryRepository->expects($this->once())
            ->method('getAllCategories')
            ->willReturn($this->createCategoryList());

        $categories = $this->categoryService->getAll();
        $this->assertNotNull($categories);
        $this->assertIsArray($categories);
        $this->assertTrue(count($categories) > 1);
    }

    /**
     * @test
     */
    public function shouldBeNotReturnAListCategories(): void
    {
        $this->categoryRepository->expects($this->once())
            ->method('getAllCategories')
            ->willReturn([]);

        $categories = $this->categoryService->getAll();
        $this->assertNull($categories);
    }

    /**
     * @test
     */
    public function shouldReturnAListCategoriesByStatusActive(): void
    {
        $this->categoryRepository->expects($this->once())
            ->method('getAllCategoriesByStatus')
            ->with(true)
            ->willReturn($this->createCategoryList());

        $categories = $this->categoryService->getAllByStatus(true);
        $this->assertNotNull($categories);
        $this->assertIsArray($categories);
        $this->assertTrue(count($categories) > 1);
    }

    /**
     * @test
     */
    public function shouldBeReturnAOneCategory(): void
    {
        $this->setCategory($this->categoryModel->id, $this->categoryModel);

        $categoryOne = $this->categoryService->getOne($this->categoryModel->id);
        $this->assertNotNull($categoryOne);
        $this->assertInstanceOf(Category::class, $categoryOne);
        $this->assertSame($this->categoryModel, $categoryOne);

    }

    /**
     * @test
     */
    public function shouldBeReturnAOneCategoryWithProductsByStatus(): void
    {
        $this->setCategory($this->categoryModel->id, $this->categoryModel);

        $totalProducts = count($this->categoryModel->products);
        $this->assertTrue($totalProducts > 0);

        $categoryOne = $this->categoryService->getOneWithProductsStatus($this->categoryModel->id, false);
        $this->assertNotNull($categoryOne);
        $this->assertInstanceOf(Category::class, $categoryOne);
        $this->assertSame($this->categoryModel, $categoryOne);

    }

    /**
     * @test
     */
    public function shouldBeDeleteAOneCategory(): void
    {
        $this->setCategory($this->categoryModel->id, $this->categoryModel);

        $this->categoryRepository->expects($this->once())
            ->method('deleteCategory')
            ->with($this->categoryModel)
            ->willReturn(true);

        $deletedCategory = $this->categoryService->delete($this->categoryModel->id);
        $this->assertTrue($deletedCategory);

    }

    /**
     * @test
     */
    public function shouldNotDeleteAOneCategory(): void
    {
        $this->setCategory(0, null);

        $this->categoryRepository->expects($this->never())
            ->method('deleteCategory')
            ->with($this->categoryModel)
            ->willReturn(false);

        $deletedCategory = $this->categoryService->delete(0);
        $this->assertFalse($deletedCategory);
    }

    /**
     * @test
     */
    public function shouldUpdateTheStatusOfACategory(): void
    {
        $this->setCategory($this->categoryModel->id, $this->categoryModel);

        $this->categoryRepository->expects($this->once())
            ->method('updateStatus')
            ->with($this->categoryModel)
            ->willReturn(true);

        $updatedCategory = $this->categoryService->updateStatusCategory($this->categoryModel->id, false);
        $this->assertTrue($updatedCategory);
        $this->assertNotSame($this->categoryModel, $updatedCategory);
    }

    /**
     * @test
     */
    public function shouldBeNotUpdateTheStatusOfACategory(): void
    {
        $this->setCategory(0, null);

        $this->categoryRepository->expects($this->never())
            ->method('updateStatus')
            ->with($this->categoryModel)
            ->willReturn(false);

        $updatedCategory = $this->categoryService->updateStatusCategory(0, false);
        $this->assertFalse($updatedCategory);
    }

    /**
     * @test
     */
    public function shouldReturnCategoriesListWithProductsByStatus(): void
    {
        $this->categoryRepository->expects($this->once())
            ->method('getAllCategories')
            ->willReturn($this->createCategoryList());

        $categories = $this->categoryService->getAllWithProductsByStatus(true);

        $this->assertNotNull($categories);

        $this->assertIsArray($categories);

        $this->assertTrue(count($categories) > 0);
    }

    /**
     * @test
     */
    public function shouldBeUpdateTheStatusOfProductOfACategory(): void
    {
        $id =$this->categoryModel->id;
        $this->setCategory($id, $this->categoryModel);

        $product = $this->categoryModel->products[0];
        $this->categoryRepository->expects($this->once())
            ->method('getProductCategory')
            ->with($this->categoryModel, $product->id)
            ->willReturn($product);

        $this->categoryRepository->expects($this->once())
            ->method('updateStatus')
            ->with($product)
            ->willReturn(true);

        $updatedProductCategory = $this->categoryService->updateStatusProductCategory($id, $product->id, false);

        $this->assertTrue($updatedProductCategory);
        $this->assertNotSame($this->categoryModel, $updatedProductCategory);
    }

    /**
     * @test
     */
    public function shouldNotUpdateTheStatusOfProductOfACategory(): void
    {
        $this->setCategory(0, null);

        $this->categoryRepository->expects($this->never())
            ->method('getProductCategory')
            ->with(null, 0)
            ->willReturn(null);

        $this->categoryRepository->expects($this->never())
            ->method('updateStatus')
            ->with(null)
            ->willReturn(false);

        $updatedProductCategory = $this->categoryService->updateStatusProductCategory(0, 0, false);

        $this->assertFalse($updatedProductCategory);
        $this->assertNotSame($this->categoryModel, $updatedProductCategory);
    }
}
