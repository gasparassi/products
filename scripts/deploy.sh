#!/bin/bash
cd ..
 git fetch --all &&
 git reset --hard origin/develop &&
 rm -r vendor/ &&
 rm composer.lock &&
 composer clear-cache &&
 composer install &&
 php artisan clear-compiled &&
 php artisan cache:clear &&
 php artisan config:clear &&
 php artisan route:clear &&
 php artisan migrate --force
