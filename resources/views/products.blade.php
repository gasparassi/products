<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Exportando tabela para pdf com Laravel</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <style>
            @page { margin:5px; }
        </style>
    </head>

    <body>
        <div class="container mt-5">
            <h2 class="text-center mb-3">Produtos</h2>

            <div class="d-flex justify-content-end mb-4">
                <a class="btn btn-primary" href="{{ route('download-csv') }}">Exportar para CSV</a>
            </div>
            <div class="d-flex justify-content-end mb-4">
                <a class="btn btn-danger" href="{{ route('download-pdf') }}">Exportar para PDF</a>
            </div>

            <table class="table table-bordered mb-5">
                <thead>
                    <tr class="table-danger">
                        <th scope="col">#</th>
                        <th scope="col">Nome</th>
                        <th scope="col">Quantidade</th>
                        <th scope="col">Status</th>
                        <th scope="col">Categoria</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($products ?? '' as $data)
                    <tr>
                        <th scope="row">{{ $data->id }}</th>
                        <td>{{ $data->name }}</td>
                        <td>{{ $data->quantity }}</td>
                        <td>{{ $data->active }}</td>
                        <td>{{ $data->category->name }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

            <div class="d-flex justify-content-center">
                {!! $products->links() !!}
            </div>

        </div>

    </body>

</html>
