<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProductExportController;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */

Route::get(
    '/',
    function () {
        return response()
            ->json(
                [
                    'message' => 'Produtos API',
                    'status' => 'Connected',
                    'statusCode' => 200,
                ],
                200
            );
    }
);

Route::group([
    'prefix' => 'v1',
], function () {
    Route::group(['prefix' => 'categories'], function () {
        // Atualiza o active (true ou false) de uma categoria
        Route::put('/{id}/update-status', [CategoryController::class, 'updateStatus']);
        // Atualiza o active do produto pela categoria vinculada de acordo com o valor informado (true ou false)
        Route::put('/{id}/update-status/product/{product_id}', [CategoryController::class, 'updateStatusProduct']);
        // Recupera as categorias de acordo com o active informado (true ou false)
        Route::get('/status', [CategoryController::class, 'indexByStatus']);
        // Recupera os produtos de acordo com o active informado (true ou false) de todas as categorias
        Route::get('/products/status', [CategoryController::class, 'indexWithProductsByStatus']);
        // Recupera os produtos de acordo com o active informado (true ou false) de uma categoria
        Route::get('/{id}/products/status', [CategoryController::class, 'showWithProductsStatus']);
    });
    Route::group(['prefix' => 'products'], function () {
        // recupera os produtos de acordo com o active informado (true ou false)
        Route::get('/status', [ProductController::class, 'indexStatus']);
        // Atualiza o active (true ou false) de um produto
        Route::put('/{id}/update-status', [ProductController::class, 'updateStatus']);
        // Dar a opção de download, em formato csv, de todos os produtos
        Route::get('/download/csv/all', [ProductExportController::class, 'indexCSV'])->name('download-csv');
        // Dar a opção de download, em formato pdf, de todos os produtos
        Route::get('/download/pdf/all', [ProductExportController::class, 'indexPDF'])->name('download-pdf');
    });
    Route::resource('categories', CategoryController::class)->except('edit', 'create');
    Route::resource('products', ProductController::class)->except('edit', 'create');
});
