<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductExportController;

// Viasualiza todos os produtos em uma tabela
Route::get('/products/all', [ProductExportController::class, 'index']);
