<?php

namespace App\Services;

use App\Repositories\Contracts\ProductRepositoryInterface;
use App\Exports\ProductDownloadCSVExport;
use App\Http\Resources\ProductWithCategoryNameResource;
use App\Exports\ProductDownloadPDFExport;

/**
 * Description of ProductService
 *
 * @author eder
 */
class ProductService
{

    private $productRepository;

    public function __construct(ProductRepositoryInterface $productService)
    {
        $this->productRepository = $productService;
    }

    private function getProducts($products)
    {
        if (count(collect($products)->toArray()) > 0) {
            return $products;
        }
        return null;
    }

    public function create(array $data)
    {
        $data['active'] = true;
        return $this->productRepository->createProduct($data);
    }

    public function getAllForDownloadCSVExports()
    {
        $products = $this->productRepository->getAllProducts();
        return count($products) > 0
            ? new ProductDownloadCSVExport(ProductWithCategoryNameResource::collection($products), 'all_products.csv')
            : null;
    }

    public function getAllForDownloadPDFExports()
    {
        $products = $this->productRepository->getAllProductsWithPaginator();
        if (count($products) > 0) {
            $pdfDownload = new ProductDownloadPDFExport($products, 'all_products.pdf');
            return $pdfDownload->collection();
        }
        return null;
    }

    public function getAll()
    {
        $products = $this->productRepository->getAllProducts();
        return $this->getProducts($products);
    }

    public function getAllStatus($value)
    {
        $products = $this->productRepository->getAllProductsByStatusStatus($value);
        return $this->getProducts($products);
    }

    public function getOne(int $id)
    {
        return $this->productRepository->getProductById($id);
    }

    public function updateProduct(int $id, array $data)
    {
        $product = $this->productRepository->getProductById($id);
        if ($product !== null) {
            return $this->productRepository->updateProduct($product, $data);
        }
        return null;
    }

    public function updateStatusProduct(int $id, bool $value)
    {
        $product = $this->productRepository->getProductById($id);
        if ($product !== null) {
            $product->active = $value;
            return $this->productRepository->updateStatusProduct($product);
        }
        return null;
    }

    public function delete(int $id)
    {
        $product = $this->productRepository->getProductById($id);
        if ($product !== null) {
            return $this->productRepository->deleteProduct($product);
        }
        return false;
    }
}
