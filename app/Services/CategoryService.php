<?php

namespace App\Services;

use App\Models\Category;
use App\Repositories\Contracts\CategoryRepositoryInterface;
use App\Services\Contracts\CategoryServiceInterface;
use Illuminate\Database\Eloquent\Collection;

/**
 * Description of CategoryService
 *
 * @author eder
 */
class CategoryService implements CategoryServiceInterface
{

    private $categoryRepository;

    public function __construct(CategoryRepositoryInterface $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    private function getCategories($categories): ?Collection
    {
        if (count(collect($categories)->toArray()) > 0) {
            return $categories;
        }
        return null;
    }

    private function getProductsByStatus(array $products, bool $value)
    {
        return array_filter($products, function ($product) use ($value) {
            return $product['active'] == $value;
        });
    }

    public function create(array $data): ?Category
    {
        $data['active'] = true;
        return $this->categoryRepository->createCategory($data);
    }

    public function getAll(): ?Collection
    {
        $categories = $this->categoryRepository->getAllCategories();

        return $this->getCategories($categories);
    }

    public function getAllByStatus($value): Collection
    {
        $categories = $this->categoryRepository->getAllCategoriesByStatus($value);
        return $this->getCategories($categories);
    }

    public function getAllWithProductsByStatus($value): Collection
    {
        $categories = $this->categoryRepository->getAllCategories();
        if (count($categories) > 0) {
            foreach ($categories as $key => $category) {
                if (count($category->products) > 0) {
                    $products = collect($category->products)->toArray();
                    $categories[$key]['products'] = $this->getProductsByStatus($products, $value);
                }
            }
        }
        return $this->getCategories($categories);
    }

    public function getOne($id): ?Category
    {
        return $this->categoryRepository->getCategoryById($id);
    }

    public function getOneWithProductsStatus($id, bool $value): ?Category
    {
        $category = $this->categoryRepository->getCategoryById($id);
        if ($category !== null && count($category->products) > 0) {
            $products = collect($category->products)->toArray();
            $category->products = $this->getProductsByStatus($products, $value);
        }
        return $category;
    }

    public function updateCategory(int $id, array $data): bool
    {
        $category = $this->categoryRepository->getCategoryById($id);
        if ($category !== null) {
            return $this->categoryRepository->updateCategory($category, $data);
        }
        return null;
    }

    public function updateStatusCategory(int $id, bool $value): bool
    {
        $category = $this->categoryRepository->getCategoryById($id);
        if ($category !== null) {
            $category->active = $value;
            return $this->categoryRepository->updateStatus($category);
        }
        return false;
    }

    public function updateStatusProductCategory(int $id, $product_id, bool $value): bool
    {
        $category = $this->categoryRepository->getCategoryById($id);
        if ($category !== null) {
            $product = $this->categoryRepository->getProductCategory($category, $product_id);
            if ($product !== null) {
                $product->active = $value;
                return $this->categoryRepository->updateStatus($product);
            }
        }
        return false;
    }

    public function delete($id): bool
    {
        $category = $this->categoryRepository->getCategoryById($id);
        if ($category !== null) {
            return $this->categoryRepository->deleteCategory($category);
        }
        return false;
    }
}
