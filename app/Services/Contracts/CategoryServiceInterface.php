<?php

namespace App\Services\Contracts;

use App\Models\Category;
use Illuminate\Database\Eloquent\Collection;

interface CategoryServiceInterface
{
    public function create(array $data): ?Category;

    public function getAll(): ?Collection;

    public function getAllByStatus(bool $value): Collection;

    public function getAllWithProductsByStatus(bool $value): Collection;

    public function getOne(int $id): ?Category;

    public function getOneWithProductsStatus(int $id, bool $value): ?Category;

    public function updateCategory(int $id, array $data): bool;

    public function updateStatusCategory(int $id, bool $value): bool;

    public function delete(int $id): bool;
}
