<?php

namespace App\Repositories;

use App\Repositories\Contracts\ProductRepositoryInterface;
use App\Models\Product;

/**
 * Description of ProductRepository
 *
 * @author eder
 */
class ProductRepository implements ProductRepositoryInterface
{

    protected $entity;

    public function __construct(Product $product)
    {
        $this->entity = $product;
    }

    /**
     * Cria um novo produto
     * @param array $product
     * @return object
     */
    public function createProduct(array $product)
    {
        return $this->entity->create($product);
    }

    /**
     * Get all Products
     * @return array
     */
    public function getAllProducts()
    {
        return $this->entity->all();
    }

    public function getAllProductsWithPaginator()
    {
        return $this->entity->paginate(10);
    }

    /**
     * Recupera todos os produtos por active (true ou false)
     * @return array
     */
    public function getAllProductsByStatusStatus(bool $value)
    {
        return $this->entity->where('active', $value)->get();
    }

    /**
     * Seleciona o Produto por ID
     * @param int $id
     * @return object
     */
    public function getProductById(int $id)
    {
        return $this->entity->find($id);
    }

    /**
     * Atualiza os dados do produto
     * @param object $product
     * @param array $data
     * @return object
     */
    public function updateProduct(object $product, array $data)
    {
        return $product->update($data);
    }

    /**
     * Deleta um produto
     * @param object $product
     */
    public function deleteProduct(object $product)
    {
        return $product->delete();
    }

    /**
     * Atualiza o status do produto para activo ou inativo
     * @param object $product
     */
    public function updateStatusProduct(object $product)
    {
        return $product->save();
    }
}
