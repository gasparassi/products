<?php

namespace App\Repositories\Contracts;

/**
 *
 * @author eder
 */
interface ProductRepositoryInterface
{

    public function getAllProducts();

    public function getAllProductsWithPaginator();

    public function getAllProductsByStatusStatus(bool $value);

    public function getProductById(int $id);

    public function createProduct(array $product);

    public function updateProduct(object $product, array $data);

    public function updateStatusProduct(object $product);

    public function deleteProduct(object $product);
}
