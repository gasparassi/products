<?php

namespace App\Repositories\Contracts;

/**
 *
 * @author eder
 */
interface CategoryRepositoryInterface
{

    public function getAllCategories();

    public function getAllCategoriesByStatus(bool $value);

    public function getCategoryById(int $id);

    public function getProductCategory(object $category, int $product_id);

    public function createCategory(array $category);

    public function updateCategory(object $category, array $data);

    public function updateStatus(object $category): bool;

    public function deleteCategory(object $category);
}
