<?php

namespace App\Repositories;

use App\Repositories\Contracts\CategoryRepositoryInterface;
use App\Models\Category;

/**
 * Description of CategoryRepository
 *
 * @author eder
 */
class CategoryRepository implements CategoryRepositoryInterface
{

    protected $entity;

    public function __construct(Category $category)
    {
        $this->entity = $category;
    }

    /**
     * Cria uma nova categoria
     * @param array $category
     * @return Category
     */
    public function createCategory(array $category)
    {
        return $this->entity->create($category);
    }

    /**
     * Deleta uma categoria
     * @param object $category
     */
    public function deleteCategory(object $category): bool
    {
        return $category->delete();
    }

    /**
     * Get all Categories
     * @return array
     */
    public function getAllCategories()
    {
        return $this->entity->all();
    }

    /**
     * Recupera todas as categorias ativas ou inativas
     * @return array
     */
    public function getAllCategoriesByStatus(bool $value)
    {
        return $this->entity->where('active', $value)->get();
    }

    /**
     * Seleciona a Categoria por ID
     * @param int $id
     * @return object
     */
    public function getCategoryById($id)
    {
        return $this->entity->find($id);
    }

    /**
     * Atualiza os dados da categoria
     * @param object $category
     * @param array $data
     * @return object
     */
    public function updateCategory(object $category, array $data)
    {
        return $category->update($data);
    }

    /**
     * Atualiza o status da categoria ou do produto da categoria
     * para ativo ou inativo
     * @param object $category
     * @return Boolean
     */
    public function updateStatus(object $category): bool
    {
        return $category->save();
    }

    /**
     * Recupera um produto de uma categoria
     * @param object $category
     * @param int $product_id
     * @return object
     */
    public function getProductCategory(object $category, int $product_id)
    {
        return $category->products->where('id', $product_id)->first();
    }
}
