<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryStoreRequest;
use App\Http\Resources\CategoryResource;
use App\Http\Requests\CategoryUpdateRequest;
use App\Http\Resources\CategoryWithProductsResource;
use App\Http\Requests\ActiveInactiveRequest;
use App\Services\Contracts\CategoryServiceInterface;
use Illuminate\Http\Response;

class CategoryController extends Controller
{

    private CategoryServiceInterface $categoryService;

    public function __construct(CategoryServiceInterface $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $categories = $this->categoryService->getAll();
            if ($categories !== null) {
                return response()->json(CategoryResource::collection($categories), Response::HTTP_OK);
            } else {
                return response()->json(['message' => 'Nenhuma categoria cadastrada.'], Response::HTTP_NOT_FOUND);
            }
        } catch (\Exception $ex) {
            return response()->json([
                'message' => 'Erro não previsto.',
                'error' => $ex->getMessage(),
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Display a listing of the resource by status (active or inactive).
     * @param \App\Http\Requests\ActiveInactiveRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function indexByStatus(ActiveInactiveRequest $request)
    {
        try {
            $categories = $this->categoryService->getAllByStatus($request->active);
            if ($categories !== null) {
                return response()->json(CategoryResource::collection($categories), Response::HTTP_OK);
            } else {
                return response()->json(['message' => 'Nenhuma categoria cadastrada.'], Response::HTTP_NOT_FOUND);
            }
        } catch (\Exception $ex) {
            return response()->json([
                'message' => 'Erro não previsto.',
                'error' => $ex->getMessage(),
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Display a listing of the resource with all products actives.
     *
     * @param \App\Http\Requests\ActiveInactiveRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function indexWithProductsByStatus(ActiveInactiveRequest $request)
    {
        try {
            $categories = $this->categoryService->getAllWithProductsByStatus($request->active);
            if ($categories !== null) {
                return response()->json(CategoryWithProductsResource::collection($categories), Response::HTTP_OK);
            } else {
                return response()->json(['message' => 'Nenhuma categoria cadastrada.'], Response::HTTP_NOT_FOUND);
            }
        } catch (\Exception $ex) {
            return response()->json([
                'message' => 'Erro não previsto.',
                'error' => $ex->getMessage(),
                'statusCode' => Response::HTTP_INTERNAL_SERVER_ERROR
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\CategoryStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryStoreRequest $request)
    {
        $fields = $request->only('name');
        try {
            $category = $this->categoryService->create($fields);
            if ($category !== null) {
                return response()->json(new CategoryResource($category), Response::HTTP_CREATED);
            } else {
                return response()->json([
                    'message' => 'Erro ao cadastrar a categoria'
                ], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
        } catch (\Exception $ex) {
            return response()->json([
                'message' => 'Erro não previsto.',
                'error' => $ex->getMessage(),
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        try {
            $category = $this->categoryService->getOne($id);
            if ($category !== null) {
                return response()->json(new CategoryWithProductsResource($category), Response::HTTP_OK);
            } else {
                return response()->json(['message' => 'Categoria não encontrada.'], Response::HTTP_NOT_FOUND);
            }
        } catch (\Exception $ex) {
            return response()->json([
                'message' => 'Erro não previsto.',
                'error' => $ex->getMessage(),
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Display the specified resource with products actives.
     *
     * @param \App\Http\Requests\ActiveInactiveRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showWithProductsStatus(ActiveInactiveRequest $request, int $id)
    {
        try {
            $category = $this->categoryService->getOneWithProductsStatus($id, $request->active);
            if ($category !== null) {
                return response()->json(new CategoryWithProductsResource($category), Response::HTTP_OK);
            } else {
                return response()->json(['message' => 'Categoria não encontrada.'], Response::HTTP_NOT_FOUND);
            }
        } catch (\Exception $ex) {
            return response()->json([
                'message' => 'Erro não previsto.',
                'error' => $ex->getMessage(),
                'statusCode' => Response::HTTP_INTERNAL_SERVER_ERROR
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\CategoryUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryUpdateRequest $request, int $id)
    {
        try {
            $category = $this->categoryService->updateCategory($id, $request->only('name'));
            if ($category) {
                return response()->json([
                    'message' => 'Categoria atualizada com sucesso.',
                ], Response::HTTP_OK);
            } else {
                return response()->json(['message' => 'Categoria não encontrada.'], Response::HTTP_NOT_FOUND);
            }
        } catch (\Exception $ex) {
            return response()->json([
                'message' => 'Erro não previsto.',
                'error' => $ex->getMessage(),
                'statusCode' => Response::HTTP_INTERNAL_SERVER_ERROR
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\ActiveInactiveRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateStatus(ActiveInactiveRequest $request, int $id)
    {
        try {
            $category = $this->categoryService->updateStatusCategory($id, $request->active);
            if ($category) {
                return response()->json([
                    'message' => 'Status da categoria atualizado com sucesso.',
                ], Response::HTTP_OK);
            } else {
                return response()->json(['message' => 'Categoria não encontrada.'], Response::HTTP_NOT_FOUND);
            }
        } catch (\Exception $ex) {
            return response()->json([
                'message' => 'Erro não previsto.',
                'error' => $ex->getMessage(),
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $category = $this->categoryService->delete($id);
            if ($category) {
                return response()->json([
                    'message' => 'Categoria removida com sucesso.',
                ], Response::HTTP_OK);
            } else {
                return response()->json(['message' => 'Categoria não encontrada.'], Response::HTTP_NOT_FOUND);
            }
        } catch (\Exception $ex) {
            return response()->json([
                'message' => 'Erro não previsto.',
                'error' => $ex->getMessage(),
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
