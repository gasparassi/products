<?php

namespace App\Http\Controllers;

use App\Services\ProductService;

class ProductExportController extends Controller
{

    private $productService;

    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    public function index()
    {
        $products = $this->productService->getAllForDownloadPDFExports();
        return view('products', compact($products));
    }


    public function indexCSV()
    {
        try {
            $products = $this->productService->getAllForDownloadCSVExports();
            if ($products !== null) {
                return $products;
            } else {
                return response()->json([
                    'message' => 'Nenhum produto cadastrado.',
                    'statusCode' => 404
                ], 404);
            }
        } catch (\Exception $ex) {
            return response()->json([
                'message' => 'Erro não previsto.',
                'error' => $ex->getMessage(),
                'statusCode' => 500
            ], 500);
        }
    }

    public function indexPDF()
    {
        try {
            $products = $this->productService->getAllForDownloadPDFExports();
            if ($products !== null) {
                return $products;
            } else {
                return response()->json([
                    'message' => 'Nenhum produto cadastrado.',
                    'statusCode' => 404
                ], 404);
            }
        } catch (\Exception $ex) {
            return response()->json([
                'message' => 'Erro não previsto.',
                'error' => $ex->getMessage(),
                'statusCode' => 500
            ], 500);
        }
    }
}
