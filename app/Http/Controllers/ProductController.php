<?php

namespace App\Http\Controllers;

use App\Services\ProductService;
use App\Http\Requests\ProductStoreRequest;
use App\Http\Requests\ProductUpdateRequest;
use App\Http\Requests\ActiveInactiveRequest;
use App\Http\Resources\ProductsWithCategoryResource;

class ProductController extends Controller
{

    private $productService;

    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $products = $this->productService->getAll();
            if ($products !== null) {
                return response()->json([
                            'data' => ProductsWithCategoryResource::collection($products),
                            'statusCode' => 200,
                                ], 200);
            } else {
                return response()->json([
                            'message' => 'Nenhum produto cadastrado.',
                            'statusCode' => 404
                                ], 404);
            }
        } catch (\Exception $ex) {
            return response()->json([
                        'message' => 'Erro não previsto.',
                        'error' => $ex->getMessage(),
                        'statusCode' => 500
                            ], 500);
        }
    }

    /**
     * Display a listing of the resource by status (active or inactive).
     *
     * @param \App\Http\Requests\ActiveInactiveRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function indexStatus(ActiveInactiveRequest $request)
    {
        try {
            $products = $this->productService->getAllStatus($request->active);
            if ($products !== null) {
                return response()->json([
                            'data' => ProductsWithCategoryResource::collection($products),
                            'statusCode' => 200,
                                ], 200);
            } else {
                return response()->json([
                            'message' => 'Nenhum produto cadastrado.',
                            'statusCode' => 404
                                ], 404);
            }
        } catch (\Exception $ex) {
            return response()->json([
                        'message' => 'Erro não previsto.',
                        'error' => $ex->getMessage(),
                        'statusCode' => 500
                            ], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\ProductStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductStoreRequest $request)
    {
        $fields = $request->only('name', 'quantity', 'category_id');
        try {
            $product = $this->productService->create($fields);
            if ($product !== null) {
                return response()->json([
                            'data' => new ProductsWithCategoryResource($product),
                            'statusCode' => 201,
                                ], 201);
            } else {
                return response()->json([
                            'message' => 'Erro ao cadastrar o produto.',
                            'statusCode' => 422
                                ], 422);
            }
        } catch (\Exception $ex) {
            return response()->json([
                        'message' => 'Erro não previsto.',
                        'error' => $ex->getMessage(),
                        'statusCode' => 500
                            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $product = $this->productService->getOne($id);
            if ($product !== null) {
                return response()->json([
                            'data' => new ProductsWithCategoryResource($product),
                            'statusCode' => 200,
                                ], 200);
            } else {
                return response()->json([
                            'message' => 'Produto não encontrado.',
                            'statusCode' => 404
                                ], 404);
            }
        } catch (\Exception $ex) {
            return response()->json([
                        'message' => 'Erro não previsto.',
                        'error' => $ex->getMessage(),
                        'statusCode' => 500
                            ], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\ProductUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductUpdateRequest $request, $id)
    {
        $fields = $request->only('category_id', 'name', 'quantity', 'active');
        try {
            $product = $this->productService->updateProduct($id, $fields);
            if ($product) {
                return response()->json([
                            'message' => 'Produto atualizado com sucesso.',
                            'statusCode' => 200,
                                ], 200);
            } else {
                return response()->json([
                            'message' => 'Produto não encontrado.',
                            'statusCode' => 404
                                ], 404);
            }
        } catch (\Exception $ex) {
            return response()->json([
                        'message' => 'Erro não previsto.',
                        'error' => $ex->getMessage(),
                        'statusCode' => 500
                            ], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\ActiveInactiveRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateStatus(ActiveInactiveRequest $request, int $id)
    {
        try {
            $product = $this->productService->updateStatusProduct($id, $request->active);
            if ($product) {
                return response()->json([
                            'message' => 'Status do produto atualizado com sucesso.',
                            'statusCode' => 200,
                                ], 200);
            } else {
                return response()->json([
                            'message' => 'Produto não encontrado',
                            'statusCode' => 404
                                ], 404);
            }
        } catch (\Exception $ex) {
            return response()->json([
                        'message' => 'Erro não previsto.',
                        'error' => $ex->getMessage(),
                        'statusCode' => 500
                            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $product = $this->productService->delete($id);
            if ($product) {
                return response()->json([
                            'message' => 'Produto removido com sucesso.',
                            'statusCode' => 200,
                                ], 200);
            } else {
                return response()->json([
                            'message' => 'Produto não encontrado',
                            'statusCode' => 404
                                ], 404);
            }
        } catch (\Exception $ex) {
            return response()->json([
                        'message' => 'Erro não previsto.',
                        'error' => $ex->getMessage(),
                        'statusCode' => 500
                            ], 500);
        }
    }
}
