<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\CategoryResource;

class ProductsWithCategoryResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->getSlugAttribute(),
            'quantity' => $this->quantity,
            'active' => $this->getActiveNameFriendly($this->active),
            'category' => new CategoryResource($this->category),
        ];
    }
}
