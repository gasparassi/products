<?php

namespace App\Exports;

use Barryvdh\DomPDF\Facade as PDF;

class ProductDownloadPDFExport
{

    private $data;
    private $fileName;

    public function __construct($data, $fileName)
    {
        $this->data = $data;
        $this->fileName = $fileName;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        // share data to view
        view()->share('products', $this->data);
        $pdf = PDF::loadView('products', $this->data);

        // download PDF file with download method
        return $pdf->download($this->fileName);
    }
}
