<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Product extends Model
{
    use HasFactory;

    protected $table = 'products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'quantity',
        'category_id',
        'active',
    ];

    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = Str::slug($value, '-');
    }

    public function getSlugAttribute(): string
    {
        return Str::slug($this->name, '-');
    }

    public function getActiveNameFriendly($value)
    {
        return $value ? 'Ativo' : 'Inativo';
    }

    public function category()
    {
        return $this->hasOne(Category::class, 'id', 'category_id');
    }
}
