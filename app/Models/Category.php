<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Product;
use Illuminate\Support\Str;

class Category extends Model
{
    use HasFactory;

    protected $table = 'categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'active',
    ];

    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = Str::slug($value, '-');
    }

    public function getActiveNameFriendly($value)
    {
        return $value ? 'Ativo' : 'Inativo';
    }

    /**
     * Recupera o nome da da categoria, atribuindo-a ao slug
     *
     * @return string
     */
    public function getSlugAttribute(): string
    {
        return Str::slug($this->name, '-');
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }
}
